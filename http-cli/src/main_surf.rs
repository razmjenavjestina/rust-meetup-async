use {
    futures::{
        stream::StreamExt,
        stream::futures_unordered::FuturesUnordered
    },
};

async fn fetch_url(uris: Vec<&str>) -> Vec<StatusCode> {
    let mut codes = Vec::new();

    let mut list_of_futures = FuturesUnordered::new();

    for uri in uris {
        list_of_futures.push(async {
            println!("[{:?}] Requesting {}", std::thread::current().id(), real_uri);
            let resp = surf::get(real_uri).await.expect("unable to fetch document");
            println!("[{:?}] Done", std::thread::current().id());
            resp.status()
        });
    }

    while let Some(res) = list_of_futures.next().await {
        codes.push(res);
    }

    codes
}

fn do_http() -> Vec<StatusCode> {
    async_std::task::block_on(
        fetch_url(vec![
            "http://httpbin.org/ip",
            "http://example.com",
            "http://n-gate.com",
        ])
    )
}

fn main() {
    println!("{:?}", do_http());
}
