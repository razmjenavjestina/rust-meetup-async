use {
    std::{
        pin::Pin,
        task::{Context, Poll},
        future::Future,
        thread,
        sync::mpsc::{channel, Sender, Receiver},
    },
    futures::executor::block_on,
};

struct MyTask {
    sender: Sender<u32>,
    receiver: Receiver<u32>,
    active: bool,
    number: u32,
}

impl MyTask {
    fn new(i: u32) -> MyTask {
        let (tx, rx) = channel();
        MyTask { number: i, sender: tx, receiver: rx, active: false }
    }
}

impl Future for MyTask {
    type Output = u32;
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        println!("[{:?}] [{}] polled", thread::current().id(), self.number);

        if !self.active {
            let waker = cx.waker().clone();
            let tx = self.sender.clone();
            let num = self.number.clone();

            let _ = thread::spawn(move || {
                thread::sleep(std::time::Duration::from_secs(num as u64));
                tx.send(num).unwrap();

                println!("[{:?}] [{}] done", thread::current().id(), num);

                waker.wake();
            });

            self.active = true;
        }

        match self.receiver.try_recv() {
            Ok(res) => Poll::Ready(res as u32),
            _       => Poll::Pending,
        }
    }
}

async fn async_main() -> (u32, u32) {
    futures::join!(MyTask::new(3), MyTask::new(0))
}

fn main() {
    let r = block_on(async_main());

    println!("----------------------------");
    println!("[{:?}] Result: {:?}", thread::current().id(), r);

    /*
    println!("----------------------------");
    let fut = MyTask::new(3);
    let r = block_on(fut);
    println!("{}", r);
    */
}
